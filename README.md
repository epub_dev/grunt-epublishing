# grunt-epublishing

> Automated front-end tasks for ePublishing Jade and client sites.

## Getting Started
This plugin requires Grunt v1.0.0.

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins.

NodeJS and Grunt-Cli must be installed before you proceed.

If you are on Mac OS X and use homebrew. The following commands will get you ready to run grunt.


```sh
brew install node
npm install -g grunt-cli
```

* To get started with using Grunt Jade you first need the right files within the project you wish to use this plugin. You will create a package.json and gruntfile.js in the root of your application. (unless someone has already done this)

* The gruntfile should contain the following code which enables the default jade task:


```js
module.exports = function(grunt) {
  // Load the grunt jade task
  grunt.loadNpmTasks('@epublishing/grunt-epublishing');

  // Run jade-default Grunt Task
  grunt.registerTask('default', ['jade']);
};
```

* The package.json file will contain the name of your site, version, and required dependencies that get installed before you can run "grunt". This file would look similar to this, and includes the actual required dependencies for Grunt Jade:


```json
{
  "name": "jade-labs",
  "version": "0.1.0",
  "description":"jade-labs",
  "repository": {
    "type": "git",
    "url": "git@bitbucket.org:epub_dev/jade"
  },
  "devDependencies": {
    "grunt": "^1.0.0",
    "grunt-jade": "git+ssh://git@bitbucket.org:epub_dev/grunt-jade.git"
  }
}
```

* Install the npm modules that this Grunt task depends on. To do this, run the following command in the application you wish to use this plugin.

```sh
npm install
```

Awesome! You are ready to run Grunt in the command line and watch Grunt Jade start working on your front-end assets. Grunt Jade will concatenate and minify the SCSS and Javascript. Here is what `grunt-jade` does when you run `grunt`:

1. `set-jade-paths`: Resolve the locations of the active Jade gem, Jade child gem (if there is one), and site directories, making them available as config variables to subsequent tasks.
1. `npm-install`: Install Node module dependencies in Jade and child gem locations if `package.json` files are present.
1. `clean`: Clean (delete) previously compiled assets, if any.
1. `babel`: Transpile standalone ES2015 scripts into public/javascripts/app with Babel.
1. `concat`: Concatenate our default JS assets into a single script, `jade.default.js`.
1. `uglify`: Minify the above script.
1. `webpack`: Compile configured modules in `app/js` and bundle them with their dependencies using Webpack, saving them to `public/javascripts/app/bundle`.
1. `sass`: Compile SCSS stylesheets to CSS using `libsass`.
1. `bless`: _(disabled by default)_ Split compiled stylesheets into chunks in order to comply with MSIE 9's stylesheet selector limit.

### Custom Tasks

`grunt-jade` provides a couple of custom Grunt tasks that run as part of the default task set:

+ `set-jade-paths` makes Grunt's configuration aware of the ePublishing gems that a site depends on.
  + Spawns a [Ruby script](./tasks/get-jade-gems.rb) as a subprocess that does the following things:
    + Sets up Bundler and loads the site's RubyGems dependencies.
    + Prints a list of gems whose names match the regular expression `/jade/` to stdout, as a JSON array.
  + Parses the JSON array and populates `grunt.config.paths` with entries corresponding to:
    + Jade's installation path as `paths.jade`
    + The child gem's installation path (if any) as `paths.jadechild`
    + Various relative asset directory paths (i.e. `paths.js_src`, `paths.css`, etc.)
+ `npm-install` iterates through the paths populated by `set-jade-paths` looking for package.json files.
  + If any results are found, it does the following for each:
    + Switches the current working directory to the parent folder of the current package.json result.
    + Runs `npm install` as a subprocess and waits for it to complete.
  + When all results have been processed, the task changes the working directory back to the site's root.
+ `watch-all` runs `grunt watch` and `grunt webpack --watch` at the same time in subprocesses

## Release Management

`grunt-epublishing` installs the [bumped](https://bumped.github.io/) package as a development dependency, and its package.json file provides an NPM script that can be used to increment release versions, commit, tag, push, and publish to NPM in a single step:

```sh
# Example (bump patch version):
$ npm run release patch
```

## Local Development

If you're working on new build system features, you can link your local clone of grunt-epublishing into a Jade site like
so:

```sh
# Inside the grunt-epublishing directory:
npm link

# Change directories to the site:
cd ../jade-site

npm link @epublishing/grunt-epublishing
```
