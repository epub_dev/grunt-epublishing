/**
 * This function merges custom SassScript functions into the base Sass config
 */

'use strict';

const sass     = require('sass');
const NODE_ENV = process.env.NODE_ENV || 'development';

module.exports = function configureSass(config) {

  config.sass.options.functions = {
    'epub-node-env()': () => new sass.String(NODE_ENV),
  };

  return config;
};
