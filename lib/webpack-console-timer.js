/* eslint-disable class-methods-use-this, no-console */

const chalk = require('chalk');

class WebpackConsoleTimer {
  apply(compiler) {
    compiler.plugin('compilation', (compilation) => {
      let startOptimizePhase;

      compilation.plugin('optimize-chunk-assets', (chunks, callback) => {
        startOptimizePhase = Date.now();
        callback();
      });

      compilation.plugin('after-optimize-chunk-assets', () => {
        const optimizePhaseDuration = Date.now() - startOptimizePhase;
        console.log(`\nOptimizer phase duration: ${chalk.bold(optimizePhaseDuration)}ms`);
      });
    });
  }
}

module.exports = WebpackConsoleTimer;
