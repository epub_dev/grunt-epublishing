/* eslint-disable no-console */

'use strict';

const _    = require('lodash');
const fs   = require('fs');
const path = require('path');

/**
 * splits a path and returns the filename (everything after the last /)
 * @param  {string} fullPath - A path to a file
 * @returns {string} the extracted filename
 */
function getFilename(fullPath) {
  if (!fullPath) {
    console.trace();
    throw new Error('fullPath was undefined');
  }
  return path.basename(fullPath);
}

 /**
 * This function merges grunt configuration objects.
 * This is a function fed into the lodash libraries
 * _.merge function. It only handles merging arrays.
 * The default is to just concat the dest array onto
 * the end of the src array.
 *
 * However if the elements in the array contain references to .js files
 * it instead checks the file name of the .js file (everything after the last /)
 * and OVERRIDES the .js file from the source if it exists in the dest
 *
 * Any src js files that are not in the dest are simply added to the end of the dest
 *
 * @param   {Object} dest The destination object to be merged
 * @param   {Object} src  The source object to be merged into the dest
 * @returns {Object}      Either the dest object or undefined to let lodash handle it.
 */
function configMerger(dest, src) {
  // We only trigger the custom merge for arrays
  if (!_.isArray(dest)) return;

  const jsRegExp = /[^*]\.js$/;

  let i, j;

  // JavaScript files with the same base name override one another:
  for (i = 0; i < dest.length; i++) {
    const destFile = getFilename(dest[i]);

    if (!jsRegExp.test(destFile)) continue;

    for (j = 0; j < src.length; j++) {
      const srcFile = getFilename(src[j]);
      if (destFile !== srcFile) continue;
      dest[i] = src[j];
    }
  }

  // All other array members are pushed and de-duped
  for (const member of src) {
    if (_.includes(dest, member)) continue;
    dest.push(member);
  }

  return dest;
}

/**
 * Merge a base Grunt configuration object with the configuration found
 * in the specified base path, if any.
 *
 * @param {Object} baseConfig - The base Grunt configuration object
 * @param {string} configDir - The root path of a site or Rails engine that contains additional configuration to be merged in
 * @returns {Object} A new configuration object
 */
module.exports = function mergeConfigs(baseConfig, configDir) {
  const configFile = path.join(configDir, 'config', 'grunt-config.js');

  if (!fs.existsSync(configFile)) return baseConfig;

  let config = require(configFile);

  if (_.isFunction(config)) {
    config = config(baseConfig);
  }

  return _.mergeWith({}, baseConfig, config, configMerger);
};
