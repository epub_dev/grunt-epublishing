/* eslint-disable max-params */
/**
 * Initializer function for creating a complete Grunt configuration object for
 * an ePublishing Jade site. Merges config values from Jade, any detected engine
 * gems, and the site itself.
 */

'use strict';

const fs               = require('fs');
const prettyjson       = require('prettyjson');
const mergeConfigs     = require('./merge-configs');
const configureWebpack = require('./configure-webpack');
const configureSass    = require('./configure-sass');
const configurePostCSS = require('./configure-postcss');

module.exports = function initJadeConfig(grunt, jadePath, jadeChildPath, jadeChildPaths) {
  // Initialize baseConfig
  let baseConfig = require('./base-config');

  baseConfig.paths.jade = jadePath;
  baseConfig.paths.jadechild = jadeChildPath;

  // Add in the jadeChildPaths
  for (const i in jadeChildPaths) {
    baseConfig.paths[i] = jadeChildPaths[i];
  }

  // Merge Jade's Grunt configuration into baseConfig
  baseConfig = mergeConfigs(baseConfig, jadePath);

  // Loop through all detected engine gem paths and merge their Grunt configurations into baseConfig
  for (const childPath of Object.values(jadeChildPaths)) {
    baseConfig = mergeConfigs(baseConfig, childPath);
  }

  // Finally, merge the site's configuration values into baseConfig
  baseConfig = mergeConfigs(baseConfig, process.cwd());

  // Update baseConfig's Webpack settings with ePublishing defaults:
  baseConfig = configureWebpack(grunt, baseConfig);

  // Add custom functions and settings to the merged Sass config
  baseConfig = configureSass(baseConfig);

  if (baseConfig.postcss) {
    baseConfig = configurePostCSS(baseConfig, grunt);
  }

  const jadeTasks = [
    'gen-tsconfig',
    'npm-install',
    'clean',
    'webpack',
    'babel',
    'concat',
    'uglify',
    'sass',
    'clean-tsconfig',
  ];

  if (baseConfig.bless) {
    jadeTasks.push('bless');
  }

  if (baseConfig.postcss) {
    jadeTasks.push('postcss');
  }

  // Initialize the grunt configuration
  grunt.config.init(baseConfig);

  // Add package.json to the grunt config
  grunt.config.set('pkg', grunt.file.readJSON('package.json'));

  // Check to see if we need to add the bower tasks
  if (fs.existsSync('bower.json')) {
    jadeTasks.unshift('bower-install-simple', 'bower');
  }

  // Register the main jade task!
  grunt.registerTask('jade', jadeTasks);

  // This is left for legacy sake. The task should be called 'jade' because that's
  // what this module is called, but some people are already using this.
  grunt.registerTask('jade-default', jadeTasks);

  // Optionally output the entire, merged config object via the --debug flag
  grunt.log.debug(prettyjson.render(grunt.config.data));
};
