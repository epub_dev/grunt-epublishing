/* eslint-disable camelcase */
/**
 * This is the default, base configuration object into which grunt-jade merges
 * additional configuration objects from Jade, engine gems, and the current site.
 */

'use strict';

const path       = require('path');
const modulesDir = path.resolve(require.resolve('lodash'), '..', '..');
const Resolver   = require('@epublishing/jade-resolver');

module.exports = {
  Resolver,
  paths: {
    app: 'app',
    css: 'public/stylesheets',
    js: 'public/javascripts',
    js_src: 'app/js',
    scss: 'app/sass',
    spec: 'spec/javascripts',
  },
  babel: {
    options: {
      sourceMap: false,
      presets: [
        [require.resolve('@epublishing/babel-preset-epublishing'), {
          transformRuntime: false,
          lodash: { cwd: modulesDir },
        }],
      ],
    },
  },
};
