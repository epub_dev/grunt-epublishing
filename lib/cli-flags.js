'use strict';

const chalk = require('chalk');
const cliui = require('cliui');

const flags = {
  debug: 'Pretty-print a representation of the complete Grunt config object',
  lint: 'Check the syntax of Webpack bundle JS files w/ eslint',
  'no-minify': 'Skip minification of Webpack bundles',
  'no-banner': 'Suppress ASCII banner',
  'no-time': 'Disable time-grunt performance stats',
  analyze: 'Output Webpack bundle stats JSON and a graphical analysis of all bundles',
  verbose: 'Show (much) more output',
  watch: 'Keep Webpack alive and re-bundle when files change',
  env: 'Sets process.env.NODE_ENV to the specified value',
};

const names = Object.keys(flags);

module.exports = function cliFlags() {
  const ui = cliui({ width: 100 });
  ui.div({ text: chalk.cyan.bold('Option Flags:'), padding: [ 1, 0, 1, 0 ] });

  for (const name of names) {
    ui.div(
      {
        text: chalk.bold(`--${name}`),
        width: 20,
        padding: [ 0, 4, 0, 4 ],
      },
      {
        text: flags[name],
        width: 80,
        padding: [ 0, 4, 0, 4 ],
      }
    );
  }

  return ui.toString();
};

module.exports.flags = flags;
