module.exports = {
  compilerOptions: {
    allowJs: true,
    experimentalDecorators: true,
    allowSyntheticDefaultImports: true,
    baseUrl: ".",
    downlevelIteration: true,
    forceConsistentCasingInFileNames: true,
    lib: ["dom", "es6"],
    module: "esnext",
    moduleResolution: "node",
    target: "es6",
  },
};
