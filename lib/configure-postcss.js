'use strict';

const cssVariables = require('postcss-css-variables');

module.exports = function configurePostCSS(config, grunt) {
  config.postcss.options = {
    map: {
      inline: false,
    },
    processors: [
      cssVariables({
        preserve: true,
      }),
    ],
  };

  return config;
};
