/**
 * This function establishes base configuration for all detected Webpack build
 * targets, including loaders and output plugins.
 */

"use strict";

const os = require("os");
const fs = require("fs");
const path = require("path");
const webpack = require("webpack");
const BundleAnalyzerPlugin =
  require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
const CompressionPlugin = require("compression-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const lodashDir = path.dirname(require.resolve("lodash"));
const moduleDir = path.dirname(lodashDir);

module.exports = function configureWebpack(grunt, config) {
  const { NODE_ENV = "development" } = process.env;
  const watch = !!grunt.option("watch");
  const verbose = !!grunt.option("verbose");
  const noMinify = !!grunt.option("no-minify");
  const analyze = !!grunt.option("analyze");
  const lint = NODE_ENV === "test" || !!grunt.option("lint");

  for (const target in config.webpack) {
    const targetConfig = config.webpack[target];

    // This makes select environment variables from the shell running Grunt available
    // in client-side JS files (with values hard-coded at compile-time). The property values
    // in the EnvironmentPlugin's configuration object function as default values, which
    // will be used if no variable corresponding to the property name can be found
    // in the user's terminal environment.
    //
    // They are accessible from bundled scripts the same way that environment variables in Node.js
    // scripts are - as properties of the `process.env` object (i.e. process.env.NODE_ENV).
    const environmentVars = new webpack.EnvironmentPlugin({
      NODE_ENV,
      DEBUG: false,
    });

    // This prevents Webpack from loading every single locale definition file that Moment.js provides.
    // 99% of the time we only care about formatting dates in US English, so we have no need for i.e.
    // the preferred date formats of Esperanto-speaking residents of Papua New Guinea. American cultural hedgemony FTW!
    const momentLocales = new webpack.ContextReplacementPlugin(
      /moment[/\\]locale$/,
      /en/
    );

    let plugins = [environmentVars, momentLocales];

    if (!config.babelLoader) {
      config.babelLoader = {
        exceptions: [],
        exclude: /(node_modules|bower_components)/,
      };
    }

    const babelExclude = config.babelLoader.exclude;
    const babelExceptions = config.babelLoader.exceptions.map(
      (mod) => new RegExp(`node_modules/${mod}/(.+)\\.js$`)
    );

    targetConfig.watch = watch;
    targetConfig.keepalive = watch || analyze;
    targetConfig.stats.modules = verbose;
    targetConfig.stats.reasons = verbose;
    targetConfig.profile = analyze;

    // Tell babel-plugin-lodash where to find modularized Lo-Dash functions:
    targetConfig.resolve.alias.lodash = lodashDir;

    const babelOptions = {
      presets: [
        [
          require.resolve("@epublishing/babel-preset-epublishing"),
          {
            lodash: { cwd: moduleDir },
            env: {
              modules: false,
            },
            minify: false,
          },
        ],
      ],
      plugins: [
        [
          require.resolve("babel-plugin-transform-react-jsx"),
          {
            pragma: "h",
            // pragmaFrag: "Fragment",
          },
        ],
      ],
    };

    const rules = [
      {
        test: /\.jsx?$/,
        loader: "babel-loader",
        exclude: (input) => {
          // Check whether the asset has a matching exclusion exception pattern and allow it to transpile if it does:
          const isException = babelExceptions.some((pattern) =>
            pattern.test(input)
          );
          if (isException) return !isException;

          // Test asset against the default exclusion pattern and return result:
          return babelExclude.test(input);
        },
        options: babelOptions,
      },
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
            options: babelOptions,
          },
          {
            loader: "ts-loader",
          },
        ],
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
      {
        test: [/\.svg$/, /\.jpe?g$/, /\.gif$/, /\.png$/],
        loader: "file-loader",
      },
    ];

    if (lint) {
      const localEslintConfig = path.join(process.cwd(), ".eslintrc");
      const globalEslintConfig = path.resolve(__dirname, "../.eslintrc");
      const eslintConfig = fs.existsSync(localEslintConfig)
        ? localEslintConfig
        : globalEslintConfig;

      rules.push({
        enforce: "pre",
        test: /\.js$/,
        exclude: /(node_modules|bower_components|public|vendor)/,
        loader: "eslint-loader",
        options: {
          configFile: eslintConfig,
          formatter: require("eslint-friendly-formatter"),
          failOnError: true,
          outputReport: {
            filePath: "eslint.xml",
            formatter: require("eslint/lib/formatters/junit"),
          },
        },
      });
    }

    targetConfig.module = { rules };

    if (analyze) {
      const bundleAnalyzer = new BundleAnalyzerPlugin({
        analyzerMode: "server",
        analyzerHost: "127.0.0.1",
        analyzerPort: "8888",
        reportFilename: "webpack-analysis.html",
        defaultSizes: "parsed",
        openAnalyzer: true,
        generateStatsFile: true,
        statsFilename: "webpack.stats.json",
        statsOptions: { chunkModules: true },
      });
      plugins.push(bundleAnalyzer);
    }

    if (!noMinify) {
      const uglifyDefaults = {
        cache: true,
        sourceMap: true,
        parallel: Math.max(os.cpus().length / 2, 1),
      };
      const { uglifyConfig = {} } = targetConfig;
      delete targetConfig.uglifyConfig;

      plugins.push(
        new UglifyJsPlugin(Object.assign({}, uglifyDefaults, uglifyConfig))
      );
    }

    if (NODE_ENV === "production") {
      plugins.push(
        new CompressionPlugin({
          asset: "[path].gz",
          algorithm: "gzip",
          test: /\.js$/,
          threshold: 10240,
          minRatio: 0.8,
        })
      );
    }

    if (Array.isArray(targetConfig.appendPlugins)) {
      plugins = plugins.concat(targetConfig.appendPlugins);
      delete targetConfig.appendPlugins;
    }

    targetConfig.plugins = plugins;

    if (
      targetConfig.customize &&
      typeof targetConfig.customize === "function"
    ) {
      config.webpack[target] = targetConfig.customize(targetConfig, webpack);
      delete config.webpack[target].customize;
    }
  }

  return config;
};
