const getGemPaths = require('@epublishing/get-gem-paths');
const flatten = require('lodash/flatten');
const merge = require('lodash/merge');
const path = require('path');
const baseConfig = require('./base-tsconfig');

/**
 * Generate an object representing all of the tsconfig.json files needed to build a site
 * @param {string} sitePath - absolute path to site
 * 
 * @typedef {Object} TsconfigData
 * @property {string} location - the absolute path of the tsconfig
 * @property {Object} tsconfig - the tsconfig object to write to file
 * 
 * @returns TsconfigData[]
 */
module.exports = function genTsConfig(sitePath = process.cwd()) {
  return getGemPaths('jade_?', sitePath)
    .then(function(gems) {
      const sourceRoots = gems
        .concat() // don't mutate input param
        .sort(function(a, _b) {
          // jade should always come last
          if (a.name === 'jade') return 1
          return -1;
        })
        .map(function(gem) { return gem.path })

      sourceRoots.unshift(sitePath) // site should always come first

      const assetLocation = 'app/js/*' // only assets in app/js should be considered.

      /** 
       * for each source root get the relative path from the root to all roots 
       * (including self) + each asset location
      */
      return sourceRoots.map((sourceRoot) => {

        /**
         * generate all of the relative paths between a given source 
         * (site, jade engine or jade child) and all other sources
        */
        const relativePaths = sourceRoots.map((root) => {
          const destination = path.resolve(root, assetLocation);
          return path.relative(sourceRoot, destination);
        })

        const pathsField = {
          compilerOptions: {
            paths: {
              '*': flatten(relativePaths),
            },
          },
        }
    
        const tsconfig = merge({}, baseConfig, pathsField)
    
        return {
          location: sourceRoot,
          tsconfig,
        }
      })
    })
}
