/* eslint-disable no-console */
'use strict';
/**
 * This task generates a tsconfig.json for the site as well as its jade engines.
 */
module.exports = function(grunt) {
  grunt.registerTask('gen-tsconfig', 'Generate .tsconfig.json for site and all Jade engines', function() {
    const genTsConfig = require('../lib/gen-tsconfig');
    const path           = require('path');
    const fs             = require('fs');

    const done = this.async();
    const tsConfigDotJson = 'tsconfig.json'

    genTsConfig(grunt.option('siteRoot'))
      .then((configPayloads) => {

        configPayloads.forEach(function(payload) {
          const configPath = path.resolve(payload.location, tsConfigDotJson);
          createTSConfig(configPath, payload.tsconfig);
        })

        done();
      })

    /**
     * creates a .tsconfig.json
     * @param {string} path - path to write to
     * @param {string} data - data to write
     */
    function createTSConfig(path, data) {
      fs.writeFileSync(path, JSON.stringify(data))
      console.log('created tsconfig.json for', path)
    }
  }
  )
}

