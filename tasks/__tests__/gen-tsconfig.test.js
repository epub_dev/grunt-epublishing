const genTsConfig = require('../../lib/gen-tsconfig');

function expectedConfig(sitePath) {

  return [{
    location: `${sitePath}`,
    tsconfig: {
      compilerOptions: {
        allowSyntheticDefaultImports: true,
        baseUrl: '.',
        lib: ['dom', 'es6'],
        module: 'esnext',
        moduleResolution: 'node',
        paths: {
          '*': [
            'app/js/*',
            '../../path/to/jade_child/app/js/*',
            '../../path/to/jade/app/js/*'
          ]
        },
        target: 'es6'
      }
    }
  }, {
    location: '/path/to/jade_child',
    tsconfig: {
      compilerOptions: {
        allowSyntheticDefaultImports: true,
        baseUrl: '.',
        lib: ['dom', 'es6'],
        module: 'esnext',
        moduleResolution: 'node',
        paths: {
          '*': [
            `../../..${sitePath}/app/js/*`,
            'app/js/*',
            '../jade/app/js/*',
          ]
        },
        'target': 'es6',
      }
    }
  }, {
    location: '/path/to/jade',
    tsconfig: {
      compilerOptions: {
        allowSyntheticDefaultImports: true,
        baseUrl: '.',
        lib: ['dom', 'es6'],
        module: 'esnext',
        moduleResolution: 'node',
        paths: {
          '*': [
            `../../..${sitePath}/app/js/*`,
            '../jade_child/app/js/*',
            'app/js/*'
          ]
        },
        'target': 'es6',
      }
    }
  }]
}


test('returns tsconfigs for pwd', function(done) {

  const processSpy = jest.spyOn(process, 'cwd').mockImplementation(() => '/my/cwd');
  genTsConfig().then(function(result) {
      expect(result).toEqual(expectedConfig('/my/cwd'));
      processSpy.mockClear()
      done()
    })
})
