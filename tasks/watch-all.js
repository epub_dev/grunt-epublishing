/**
 * This allows both the standard watch task and the Webpack watcher to
 * run concurrently
 */

'use strict';

module.exports = (grunt) => {
  grunt.registerTask('watch-all', 'Watch with Sass and Webpack concurrently', function() {
    const done = this.async();
    const { spawn } = require('child_process');
    const cwd = process.cwd();
    const { env } = process;
    const [ nodeBin, gruntBin, ...argv ] = process.argv;
    const flags = '--no-banner --no-time';
    const tasks = [
      `watch ${flags}`,
      `webpack --watch ${flags}`,
    ];

    grunt.log.ok('Spawning watcher tasks...');

    const subprocesses = tasks.map((task) => {
      const args = [ gruntBin, ...task.split(' ') ];
      const subprocess = spawn(nodeBin, args, {
        cwd,
        env,
        stdio: 'inherit',
      });

      subprocess.on('error', (err) => {
        grunt.log.fatal(err);
      });

      return subprocess;
    });

    const cleanup = (exit = false) => {
      grunt.log.ok('Killing subprocesses...');
      for (const subprocess of subprocesses) {
        subprocess.kill('SIGKILL');
      }
      if (exit) process.exit();
    };

    process.on('exit', cleanup);
    process.on('SIGINT', () => cleanup(true));
  });
};
