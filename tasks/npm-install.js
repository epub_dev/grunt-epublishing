/**
 * This registers a grunt task that performs an NPM install at each level of
 * hierarchy that has a package.json file present (jade and child gem). Site
 * level NPM installs are still manual.
 */

'use strict';

module.exports = function(grunt) {
  const _        = require('lodash');
  const async    = require('async');
  const path     = require('path');
  const Resolver = require('@epublishing/jade-resolver');

  grunt.registerTask('npm-install', 'Install Node module dependencies', function() {
    const Spinner    = require('cli-spinner').Spinner;
    const origPath   = process.cwd();
    const resolver   = new Resolver(grunt.config.get('paths'));
    const isTerminal = Boolean(process.stdout.isTTY);

    resolver.removePath('site');

    const packages = resolver.find('package.json');

    if (_.isEmpty(packages)) {
      grunt.log.ok('No other package.json files found in hierarchy. Skipping npm-install task.');
      return;
    }

    const done = this.async();
    const installQueue = _.map(packages, (pkg) => {
      const basePath = path.dirname(pkg);
      const basename = path.basename(basePath);

      return (callback) => {
        const spinner = new Spinner('%s installing...');

        spinner.setSpinnerString(Spinner.spinners[18]);

        grunt.log.writeln(`Installing NPM modules in ${basename}`);

        if (isTerminal) spinner.start();

        process.chdir(basePath);

        grunt.util.spawn({
          cmd: 'npm',
          args: [ 'ci' ],
        }, (err, result) => {
          if (err) {
            grunt.fail.warn(err);
          } else {
            if (isTerminal) spinner.stop(true);
            grunt.verbose.writeln(result.stdout);
            grunt.log.ok('Success!');
          }

          callback(err, result);
        });
      };
    });

    async.series(installQueue, (err, results) => {
      if (err) {
        return done(err);
      }

      grunt.log.ok('All NPM packages installed');
      process.chdir(origPath);
      done(results);
    });
  });
};
