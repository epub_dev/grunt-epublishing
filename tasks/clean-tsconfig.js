/* eslint-disable no-console */
'use strict';
/**
 * Remove tsconfig from site and jade engines
 */
module.exports = function (grunt) {
  grunt.registerTask('clean-tsconfig', 'Remove tsconfig from site and jade engines', function() {
    const genTsConfig = require('../lib/gen-tsconfig');
    const fs   = require('fs')
    const path = require('path')

    const { NODE_ENV } = process.env;

    /**
     * if the build is not for a deployment (i.e. development), we'll want to keep tsconfigs
     * a better developer experience
    */
    if ( NODE_ENV !== 'production' && NODE_ENV !== 'staging' ) {
      console.log('Not deploying to production or stage. tsconfig.json will not be removed.');
      return;
    }

    const done = this.async();
    
    genTsConfig(grunt.option('siteRoot'))
      .then((configPayloads) => {
        configPayloads.forEach(function(payload) {
          const configPath = path.resolve(payload.location, 'tsconfig.json');
          fs.unlinkSync(configPath)
        })
        done();
      }).catch((err) => {
        console.log(err)
        done()
      })
  })
}
