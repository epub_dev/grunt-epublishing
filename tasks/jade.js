/* eslint-disable no-console */

if (typeof globalThis === 'undefined') {
  (function() {
    if (typeof self !== 'undefined') { self.globalThis = self; }
    else if (typeof window !== 'undefined') { window.globalThis = window; }
    else if (typeof global !== 'undefined') { global.globalThis = global; }
    else {
      Object.defineProperty(Object.prototype, 'globalThis', {
        get: function() {
          return this;
        },
        configurable: true
      });
    }
  })();
}

'use strict';

const path           = require('path');
const timeGrunt      = require('time-grunt');
const jitGrunt       = require('jit-grunt');
const fs             = require('fs');
const readPkg        = require('read-pkg');
const getGemPaths    = require('@epublishing/get-gem-paths');
const cliFlags       = require('../lib/cli-flags');
const initJadeConfig = require('../lib/init-jade-config');

module.exports = function(grunt) {
  grunt.option('siteRoot', process.cwd())

  if (!grunt.option('no-time')) timeGrunt(grunt);
  jitGrunt(grunt, {
    'install-eslint': '@epublishing/grunt-install-eslint',
  });

  /**
   * This registers a grunt task which shells out and uses bundler to
   * determine the paths to the jade gem and any jade child engine gem
   */
  grunt.registerTask('set-jade-paths', 'Get Jade Gem Paths', function() {
    let jadePath;
    let jadeChildPath;
    const jadeChildPaths = {};
    const isTerminal     = Boolean(process.stdout.isTTY);
    const moduleRoot     = path.resolve(__dirname, '..');
    const done           = this.async();
    const asciiBanner    = fs.readFileSync(path.join(moduleRoot, 'etc/banner.txt'), 'utf8');
    const modulePkg      = readPkg.sync({ cwd: moduleRoot });
    const sitePkg        = readPkg.sync();
    const banner = [
      asciiBanner,
      `grunt-epublishing v${modulePkg.version}`,
      `currently building: ${sitePkg.name} v${sitePkg.version}`,
      cliFlags(),
    ].join('\n');

    if (grunt.option('env')) {
      process.env.NODE_ENV = grunt.option('env');
    }

    if (isTerminal && !grunt.option('no-banner')) grunt.log.writeln(`\n${banner}`);

    // Query bundler for Jade-related gems in a subprocess
    getGemPaths('jade_?')
      .then((data) => {
        for (const gem of data) {
          if (gem.name === 'jade') {
            jadePath = gem.path;
          } else if ((/^jade_/).test(gem.name)) {
            jadeChildPaths[gem.name] = gem.path;
            jadeChildPath = gem.path;
          }
        }

        done();
        initJadeConfig(grunt, jadePath, jadeChildPath, jadeChildPaths);
      })
      .catch((err) => {
        grunt.fail.fatal(err);
        done();
      });
  });

  // Force-run the `set-jade-paths` task
  grunt.task.run(['set-jade-paths']);
};
